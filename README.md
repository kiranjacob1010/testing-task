### Api automated Test

This repository contains a set of automated api tests to verify the apis of https://reqres.in/ 
  
#### Tool Used
Postman REST Client

#### Requirements
Postman REST Client App must be installed in the testing computer  
Note: This test will not work with deprecated postman chrome extension

#### How To run
clone/download the repository to the testing machine.  
Import the postman collection file (reqres_api_tests.postman_collection.json) to the postman REST client app  
Run the collection "reqres_api_tests" and verify the result